METEOR
Meteor is an ultra-simple environment for building modern web applications.
 It is a sleek, intuitive, and powerful front-end framework for faster and easier web development, created by Mark Otto and Jacob Thornton, and maintained by the core team with the massive support and involvement of the community.

With Meteor you write apps:
•	in pure JavaScript
•	that send data over the wire, rather than HTML
•	using your choice of popular open-source libraries
 For more information, read the guide or the reference documentation at http://docs.meteor.com/.
Quick Start
 Step 1 : Meteo install on different OS 
- On Windows, simply go to https://www.meteor.com/install and use the Windows installer.
- On Linux/macOS, use this line: 
curl https://install.meteor.com/ | sh

Step 2 : Creating a project 
meteor create nom_projet

Step 3 : Run the project 
cd nom_projet

meteor
Ready to Go!
Your local Meteor checkout is now ready to use! You can use this ./meteor anywhere you would normally call the system Meteor. 
Uninstalling Meteor
Aside from a short launcher shell script, Meteor installs itself inside your home directory. To uninstall Meteor, run:
rm -rf ~/.meteor/
sudo rm /usr/local/bin/meteor

On Windows, just run the uninstaller from your Control Panel.
Developer Resources
Building an application with Meteor?
•	Announcement list: sign up at http://www.meteor.com/
•	Having problems? Ask for help at: http://stackoverflow.com/questions/tagged/meteor
•	Discussion forums: https://forums.meteor.com/
Interested in contributing to Meteor?
•	Issue tracker: https://github.com/meteor/meteor/issues
•	Contribution guidelines: https://github.com/meteor/meteor/tree/devel/Contributing.md
We are hiring! Visit https://www.meteor.com/jobs to learn more about working full-time on the Meteor project.

For now we can use only HTML, CSS and JS with Meteor but it's not sufficient to use the packages Bootstrap and Jquery.
This site helped us findind the appropriate commands line to use on Meteor :
https://atmospherejs.com/ 
the commandes line that we used in our project are :
	meteor add twbs:bootstrap
meteor add reactive-var
	This package provide ReactiveVar, a general-purpose reactive datatype for use with tracker. ReactiveVaris  documented on the main Meteor docs page.
Finally, we replace these files in our project (client, server, collections)
